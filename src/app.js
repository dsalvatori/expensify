import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import AppRouter from './routers/AppRouter'
import configureStore from './store/configureStore';
import {addExpense,editExpense,removeExpense,startSetExpenses} from './actions/expenses';
import {setEndDate,setTextFilter,setStartDate,sortByAmount,sortByDate} from './actions/filters';
import getVisibleExpenses from './selectors/expenses';
import 'normalize.css/normalize.css';
import './styles/styles.scss';



const store = configureStore();



store.dispatch(startSetExpenses());

// store.dispatch(addExpense({description:"Water Bill",amount:300}));
// store.dispatch(addExpense({description:"Gas Bill",amount:200,createdAt:50}));
// store.dispatch(addExpense({description:"Rent",amount:100000}));


const jsx = (
    <Provider store={store}>
        <AppRouter/>
    </Provider>
);

ReactDOM.render(jsx,document.getElementById('app'));






