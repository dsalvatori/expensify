//Object desctructuring


// const person = {
//     name:'Andrew',
//     age:26,
//     location:{
//         city:'Philadelfia',
//         temp:92
//     }
// }

// const {name:firstName = 'Anonymous',age} = person;
// // const name = person.name;
// // const age = person.age;

// console.log(`${firstName} is ${age}`)


// const {city,temp:temperature} = person.location;

// if(city && temperature){
//     console.log(`It's ${temperature} in ${city}`)
// }


// const book = {
//     title:'Ego is the Enemy',
//     author:'Ryan Holiday',
//     publisher:{
//      //   name:'Penguin'
//     }
// }

// const {name:publisherName = 'Self-Published'} = book.publisher;

// console.log(publisherName)


//Array destructuring

const address = ['H. Lagos 782','Boulogne','Buenos Aires','1609'];

const [, city, state = 'Cordoba'] = address

console.log(`You are in ${city} ${state}`)

const item = ['Coffee (hot)','$2.00','$2.50','$2.75']

const [product,smallCost,mediumCost,largeCost] = item

console.log(`A medium ${product} costs ${mediumCost}`);
