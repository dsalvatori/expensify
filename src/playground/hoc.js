// Higher Order Component (HOC) - A component (HOC) that renders another component 
// Reuse code
// Render Hijacking
// Prop manipulation
// Abstract state


import React from 'react';
import ReactDOM  from 'react-dom';

const Info = (props) => (
    <div>
        <h1>Info</h1>
        <p>The info is: {props.info}</p>
    </div>

)

const withAdminWarning = (WrappedComponent) => {

    return (props) => (
        <div>
            {props.isAdmin && <p>This is priviledged info. Please dont share!</p>}
            <WrappedComponent {...props}/>
        </div>
    )
}

const AdminInfo = withAdminWarning(Info);

const requireAuthentication = (WrappedComponent) => {
    return (props) => (
        <div>
            {!props.isAuthenticated && <p>Please log in to view Info</p>}
            {props.isAuthenticated && <WrappedComponent {...props}/>}
        </div>
    )
}

const AuthInfo = requireAuthentication(Info);

 

//ReactDOM.render(<AdminInfo isAuthenticated={false} info="These are the details"/>,document.getElementById('app'));
ReactDOM.render(<AuthInfo isAuthenticated={true} info="These are the details"/>,document.getElementById('app'));