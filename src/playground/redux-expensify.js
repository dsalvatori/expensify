import {createStore, combineReducers} from 'redux';
import uuid from 'uuid';












store.subscribe( () => {
    const state = store.getState();
    const visibleExpenses = getVisibleExpenses(state.expenses,state.filters)
    console.log(visibleExpenses)
    
})


const expenseOne = store.dispatch(addExpense({description:'Expensa',amount:3,createdAt:1000}))
const expenseTwo = store.dispatch(addExpense({description:'Coffee',amount:33,createdAt:-1000}))



// store.dispatch(removeExpense(expenseOne.expense));
// store.dispatch(editExpense(expenseTwo.expense.id, {amount:100}) )
// store.dispatch(setTextFilter('expensa'));
// store.dispatch(setTextFilter());

store.dispatch(sortByAmount());
// store.dispatch(sortByDate());


// store.dispatch(setStartDate(0));
// store.dispatch(setStartDate());
// store.dispatch(setEndDate(250));


const demoState = {

    expenses: [{
        id: '23423',
        description: 'January Rent',
        note: 'note',
        amount: 5400,
        createdAt:0
    }],
    filters:{
        text:'rent',
        sortBy: 'amount', //date or amount
        startDate: undefined,
        endDate: undefined
    }

}


const user = {
    name: 'Jen',
    age: 24
}

console.log({
    ...user,  
})

