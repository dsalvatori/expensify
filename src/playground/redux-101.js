import {createStore} from 'redux';

//Action generators - functions that return action objects

const incrementCount = ({incrementBy = 1} = {}) => ({type:'INCREMENT',incrementBy});

const decrementBy = ({decrementBy = 1} = {}) => ({type: 'DECREMENT', decrementBy});

const setCount = ({count}) => ({type: 'SET',count})

const resetCount = () => ({type:'RESET'});

//Reducers

// 1. Reducer are pure functions (depend only on inputs and does not modify environment)
// 2. Never change state or action directly, provide new object to modify state



const countReducer = (state = {count:0},action) => {

    switch (action.type){
        case 'INCREMENT':
            return {count:state.count + action.incrementBy};
        case 'DECREMENT':
            return {count:state.count - action.decrementBy};
        case 'SET':
            return {count:action.count};
        case 'RESET':
            return {count:0};
        default:
            return state;
    }

}

const store = createStore(countReducer);


const unsubscribe = store.subscribe( () => {
    console.log(store.getState());
});



//Increment count
store.dispatch(incrementCount({incrementBy:5}));



store.dispatch(incrementCount());



store.dispatch(decrementBy({
    type:'DECREMENT',
    decrementBy:10
}));

store.dispatch(decrementBy());

store.dispatch(setCount({
    type:'SET',
    count:20
}));

store.dispatch(resetCount());


