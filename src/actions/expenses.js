import axios from 'axios'
import uuid  from 'uuid';


//ADD_EXPENSE

export const addExpense = (expense) => ({
    type:'ADD_EXPENSE',
    expense
});


export const startAddExpense = (expenseData = {}) => {

    return (dispatch) => {
    
        const {
            description = '',
            note = '',
            amount = 0,
            createdAt = 0
        } = expenseData;
    
        const expense = {description,note,amount,createdAt};
        
        axios.post("http://sheltered-forest-47297.herokuapp.com/api/expenses",expense).then(res => {
            if(res.status === 200){
                dispatch(addExpense({id:res.data.id,...expense}))
            }
    
        }).catch( e => {
            console.log(e)
        })
           
    }

} 


//REMOVE_EXPENSE

export const removeExpense = ( {id} = {}) => ({type:'REMOVE_EXPENSE',id});

//EDIT_EXPENSE

export const editExpense = ( id, updates ) => (
    {
        type:'EDIT_EXPENSE',
        id,
        updates
    }
)

export const startEditExpense = (id, updates) => {
    return async (dispatch) => {
        try{
            const res = await axios.put(`http://sheltered-forest-47297.herokuapp.com/api/expenses/${id}`,updates);
            if(res.status === 200){
                dispatch(editExpense(id,updates))
            } 
        }catch(e){
            alert(e.message)
        }
    }
}


export const setExpenses = (expenses) => ({
    type:'SET_EXPENSES',
    expenses
});


export const startSetExpenses = () => {

    return async (dispatch) => {
        try{
            const res = await axios.get("http://sheltered-forest-47297.herokuapp.com/api/expenses");
            if(res.status === 200){
                dispatch(setExpenses(res.data))
            } 
        }catch(e){
            alert(e.message)
        }
    }
} 