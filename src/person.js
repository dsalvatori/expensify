export const isAdult = (age) => age > 17;

export const canDrink = (age) => age > 17;

export default (age) => age > 64; 