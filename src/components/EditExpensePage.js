import React from 'react';;
import {connect} from 'react-redux';
import {startEditExpense,removeExpense} from '../actions/expenses';
import ExpenseForm from '../components/ExpenseForm';


const EditExpensePage = (props) => {
    return (
        <div>
            Editing Expense Nr {props.match.params.id}

            <ExpenseForm
            expense={props.expense}
            onSubmit={(updates) => {
                props.dispatch(startEditExpense(props.match.params.id,updates));
                props.history.push('/');
            }}
        />
        <button onClick={() => {
            props.dispatch(removeExpense({id:props.expense.id}));
            props.history.push('/');
        }}>Remove</button>
        </div>
    );
}

const mapStateToProps = (state,props) => {
    return {
        expense:state.expenses.find( expense => expense.id === props.match.params.id)
    }
};
export default connect(mapStateToProps)(EditExpensePage);