const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');


module.exports = (env) => {
    const isProduction = env === 'production';
    const CSSExtract = new ExtractTextPlugin('styles.css');

    console.log('env',env)
    return { 
        entry: ["babel-polyfill","./src/app.js"],
        output:{
            path:path.join(__dirname,'public','dist'),
            filename:"bundle.js"
        },
        module:{
            rules:[{
                loader:'babel-loader', //solo uso un solo loader para esta regla
                test:/\.js$/,
                exclude:/node_modules/
            },{
                test:/\.s?css$/,
                use:CSSExtract.extract({
                    use:[
                        {
                            loader:'css-loader',
                            options:{
                                sourceMap:true
                            }
                        },
                        {
                            loader:'sass-loader',
                            options:{
                                sourceMap:true
                            }
                        }
                    ] //no uso mas style-loader por que era para hacer los styles inline
                }) //con use puedo usar mas de un loader. Los loaders aceptan opciones
            }]
        },
        plugins:[
            CSSExtract
        ],
        devtool:isProduction ? 'source-map' : 'inline-source-map', //uso inline-source-map para tenerlos en dev tambien una vez que los saque de bundle.js
        devServer:{
            contentBase:path.join(__dirname,'public'),
            historyApiFallback: true, //para que cuando busque una subruta no me tire 404
            publicPath:'/dist/' //el dev server me genera la carpeta virtualmente donde ubica los assets (bundles)
        }
    }
}

